package counter

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// TestCounter - tests `Counter` using discrete events.
func TestCounter(t *testing.T) {
	collectionPeriod := time.Millisecond * 100
	counter := New(collectionPeriod)

	t10 := time.NewTimer(10 * time.Millisecond)   // adding first event
	t20 := time.NewTimer(20 * time.Millisecond)   // checking the number of events - should be equal to 1
	t70 := time.NewTimer(70 * time.Millisecond)   // adding the second event
	t101 := time.NewTimer(101 * time.Millisecond) // checking the number of events - should be equal to 2
	t121 := time.NewTimer(121 * time.Millisecond) // checking the number of events - should be equal to 1
	t181 := time.NewTimer(181 * time.Millisecond) // checking the number of events - should be equal to 0

checkingLoop:
	for {
		select {
		case <-t10.C:
			counter.CreateEvent()
		case <-t20.C:
			assert.Equal(t, 1, counter.CountEvents(), "check at 20ms")
		case <-t70.C:
			counter.CreateEvent()
		case <-t101.C:
			assert.Equal(t, 2, counter.CountEvents(), "check at 101ms")
		case <-t121.C:
			assert.Equal(t, 1, counter.CountEvents(), "check at 111ms")
		case <-t181.C:
			assert.Equal(t, 0, counter.CountEvents(), "check at 181ms")
			break checkingLoop
		}
	}
}

// TestCounter1 - tests `Counter` using stream of events.
func TestCounter1(t *testing.T) {

	collectionPeriod := time.Second
	counter := New(collectionPeriod)

	// let's assume that we start at the time offset 0 and use following four time periods:
	// 0ms - 500ms: we will be adding new events, so the number of events should constantly increase,
	// 501ms - 1000ms: we will not add new events, so the number of events should remain the same,
	// 1001ms - 1500ms: we will not add new events, so the number of events should constantly decrease,
	// 1501ms - 1600ms: the number of events should be equal to zero.

	startTime := time.Now()

	// 0ms - 500ms: we will be adding new events, so the number of events should constantly increase
	firstPeriod := startTime.Add(time.Millisecond * 500)
	var previousCount, currentCount int
	for currentTime := time.Now(); currentTime.Before(firstPeriod); currentTime = time.Now() {
		previousCount = currentCount
		counter.CreateEvent()
		currentCount = counter.CountEvents()
		assert.True(t, currentCount > previousCount,
			"during the first period the number of events should steadily rise")
	}

	// 501ms - 10000ms: we will not add new events, so the number of events should remain the same
	secondPeriod := firstPeriod.Add(time.Millisecond * 500)
	for currentTime := time.Now(); currentTime.Before(secondPeriod); currentTime = time.Now() {
		previousCount = currentCount
		currentCount = counter.CountEvents()
		assert.Equal(t, currentCount, previousCount,
			"during the second period the number of events should remain the same")
	}

	// 1001ms - 1500ms: we will not add new events, so the number of events should constantly decrease
	thirdPeriod := secondPeriod.Add(time.Millisecond * 500)
	for currentTime := time.Now(); currentTime.Before(thirdPeriod); currentTime = time.Now() {
		previousCount = currentCount
		currentCount = counter.CountEvents()
		assert.True(t, currentCount <= previousCount,
			"during the third period the number of events should steadily decline")
	}

	// 1501ms - 1600ms: the number of events should be equal to zero
	fourthPeriod := thirdPeriod.Add(time.Millisecond * 100)
	for currentTime := time.Now(); currentTime.Before(fourthPeriod); currentTime = time.Now() {
		count := counter.CountEvents()
		assert.True(t, count == 0, "during the fourth period the number of events should be equal to zero")
	}
}

// TestCounter_AddEvents - tests `AddEvents` method using stream of events.
func TestCounter_AddEvents(t *testing.T) {
	numberOfEvents := 10
	events := make([]time.Time, numberOfEvents)
	for i := 0; i < numberOfEvents; i++ {
		events[i] = time.Now()
	}

	countingPeriod := time.Millisecond * 100
	counter := New(countingPeriod)
	counter.AddEvents(events)
	assert.Equal(t, numberOfEvents, counter.CountEvents(), "none of the events should have expired")

	expiringPeriod := time.Now().Add(countingPeriod)
	var currentCount int
	previousCount := counter.CountEvents()
	for currentTime := time.Now(); currentTime.Before(expiringPeriod); currentTime = time.Now() {
		currentCount = counter.CountEvents()
		assert.True(t, previousCount >= currentCount,
			"during the expiring period the number of events should steadily decline")
		previousCount = counter.CountEvents()
	}
	currentCount = counter.CountEvents()
	assert.Equal(t, 0, currentCount,
		"after the expiring period the number of events is not equal to zero")
}

// TestCounter_AllEvents - tests AllEvents method.
func TestCounter_AllEvents(t *testing.T) {
	counter := New(time.Millisecond * 200)
	events := make([]time.Time, 10)
	for i := 0; i < 10; i++ {
		events[i] = time.Now()
	}
	counter.AddEvents(events)
	retrievedEvents := counter.AllEvents()
	assert.Equal(t, events, retrievedEvents, "events and retrievedEvents should be equal")
}
