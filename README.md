
## Task

Using only the standard library, create a Go HTTP server that on each request responds with a counter of the total number of requests that it has received during the previous 60 seconds (moving window). The server should continue to the return the correct numbers after restarting it, by persisting data to a file.

## Assumptions

1. The requirement of using only the standard library does not apply to writing tests (I used package testify).

2. The current request is also counted, i.e the first request will receive `{"received_requests": 1}`, but not
`{"received_requests": 0}`.

3. Requests to `/favicon.ico` are not counted.

## Packages

**package domain** - provides core interfaces: 
* `Counter` - counts the number of events occurred during the given time interval (mowing window).
* `DB` - stores recorded events.
* `Server` - a server that on each request responds with a counter of the total number of requests received during 
    the given time interval (moving window). Server internally uses interface `Counter` to count the number of 
    occurred events (in this case an event is a single request) and `DB` interface to store recorded events.
* `Limiter` - limits requests.    
    
**package counter** - implements `domain.Counter`. Implementation is not concurrently safe by itself and is supposed to be used with sync.Mutex. 

**package filedb** - implements `domain.DB`.

**package server** - implements `domain.Server`.

**package tests** -  tests integration of packages `counter`, `filedb` and `server`.

**package cmd** - contains an executable file `recounter.go`. 

**package limiter** - implements `domain.Limiter`

The implementation of any high-level packages (`filedb`, `server`, `counter`) can be changed without affecting any 
other package. 

## Curl commands for a presentation

curl -v --header "X-Forwarded-For: 192.168.0.0" localhost:8080
sleep 20s && curl -v --header "X-Forwarded-For: 192.168.0.0" localhost:8080

curl -v --header "X-Forwarded-For: 192.168.0.1" localhost:8080
sleep 20s && curl -v --header "X-Forwarded-For: 192.168.0.1" localhost:8080
