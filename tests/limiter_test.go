package recounter_test

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"testing"
	"time"

	"gitlab.com/ayzu/recounter/domain"

	"gitlab.com/ayzu/recounter/limiter"

	"github.com/stretchr/testify/require"
	"gitlab.com/ayzu/recounter/counter"
	"gitlab.com/ayzu/recounter/filedb"
	"gitlab.com/ayzu/recounter/server"
)

const (
	countingPeriod = time.Second * 10
	header         = "X-Forwarded-For"
	ip1            = "192.0.0.1"
	ip2            = "192.0.0.2"
	limit          = 2
)

// getFreePort - obtain a free open TCP port that is ready to use.
func getFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}
	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}

func TestLimiter(t *testing.T) {
	port, err := getFreePort()
	require.NoError(t, err)
	portString := strconv.Itoa(port)

	cntr := counter.New(countingPeriod)
	file, err := ioutil.TempFile("", "")
	defer os.Remove(file.Name())
	require.Nil(t, err)
	fdb, err := filedb.New(file.Name())
	require.Nil(t, err)
	lmtr := limiter.New(countingPeriod, limit, domain.LimitByHeader)

	srvr, err := server.New(cntr, lmtr, fdb, "", portString)
	require.Nil(t, err)

	baseUrl := fmt.Sprintf("http://localhost:%v", portString)
	resource := "/someResource"
	url := baseUrl + resource
	log.Println(url, srvr.Addr)

	client1 := &http.Client{}
	req1, err := http.NewRequest(http.MethodGet, url, nil)
	require.NoError(t, err)
	req1.Header.Add(header, ip1)

	client2 := &http.Client{}
	req2, err := http.NewRequest(http.MethodGet, url, nil)
	require.NoError(t, err)
	req2.Header.Add(header, ip2)

	go func() {
		srvr.Serve(resource)
	}()

	// first two requests for each client should return OK
	for i := 0; i < 2; i++ {
		resp, err := client1.Do(req1)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, resp.StatusCode)
		resp, err = client2.Do(req2)
		require.Equal(t, http.StatusOK, resp.StatusCode)
		require.NoError(t, err)
	}

	// next requests should return TooManyRequests
	resp, err := client1.Do(req1)
	require.NoError(t, err)
	require.Equal(t, http.StatusTooManyRequests, resp.StatusCode)
	resp, err = client2.Do(req2)
	require.NoError(t, err)
	require.Equal(t, http.StatusTooManyRequests, resp.StatusCode)

	// wait for countingPeriod
	time.Sleep(countingPeriod)

	// after waiting first two requests for each client should return OK
	for i := 0; i < 2; i++ {
		resp, err := client1.Do(req1)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, resp.StatusCode)
		resp, err = client2.Do(req2)
		require.Equal(t, http.StatusOK, resp.StatusCode)
		require.NoError(t, err)
	}

	// next requests should return TooManyRequests
	resp, err = client1.Do(req1)
	require.NoError(t, err)
	require.Equal(t, http.StatusTooManyRequests, resp.StatusCode)
	resp, err = client2.Do(req2)
	require.NoError(t, err)
	require.Equal(t, http.StatusTooManyRequests, resp.StatusCode)
}

func TestLimiter1(t *testing.T) {
	port, err := getFreePort()
	require.NoError(t, err)
	portString := strconv.Itoa(port)

	cntr := counter.New(countingPeriod)
	file, err := ioutil.TempFile("", "")
	defer os.Remove(file.Name())
	require.Nil(t, err)
	fdb, err := filedb.New(file.Name())
	require.Nil(t, err)
	lmtr := limiter.New(countingPeriod, limit, domain.LimitBySource)

	srvr, err := server.New(cntr, lmtr, fdb, "", portString)
	require.Nil(t, err)

	baseUrl := fmt.Sprintf("http://localhost:%v", portString)
	resource := "/someResource"
	url := baseUrl + resource
	log.Println(url, srvr.Addr)

	client1 := &http.Client{}
	req1, err := http.NewRequest(http.MethodGet, url, nil)
	require.NoError(t, err)
	req1.Header.Add(header, ip1)
	client1.Do(req1)

	client2 := &http.Client{}
	req2, err := http.NewRequest(http.MethodGet, url, nil)
	require.NoError(t, err)
	req2.Header.Add(header, ip2)
	client2.Do(req2)

	go func() {
		srvr.Serve(resource)
	}()

	resp, err := client1.Do(req1)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.StatusCode)
	resp, err = client2.Do(req2)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.StatusCode)

	resp, err = client1.Do(req1)
	require.NoError(t, err)
	require.Equal(t, http.StatusTooManyRequests, resp.StatusCode)

	resp, err = client2.Do(req2)
	require.NoError(t, err)
	require.Equal(t, http.StatusTooManyRequests, resp.StatusCode)
}
